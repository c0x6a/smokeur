=======
Smokeur
=======

An API only file uploader, inspired in `imgur.com <https://imgur.com>`_
using `FastAPI <https://fastapi.tiangolo.com/>`_, made for my personal
use only, but you can fork it and use yourself too (and improve it if
you want, MRs are more than welcome).

**Why the name?** I wasn't inspired to choose a name so I got the first
word of a song I was listening the moment I was writing it, and added
the ``ur``  because this project is (or intended to be) a clone-like of
``imgur``, so it ended up in the ``Smokeur`` name.


How to install and run
----------------------

Clone this repository, create a virtual environment and install all
the dependencies:


.. code-block:: shell

   pip install -r requirements.txt


Configure environment variables and run
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Rename ``env.sample`` to ``.env`` and change the needed values, after
that just run:


.. code-block:: shell

   uvicorn main:app --reload


and open your web broswser to:
`127.0.0.1:8000/docs <http://127.0.0.1:8000/docs>`_ to see it running.


Serving with NGINX
------------------

There's also a ``sample.nginx`` file to help you configure this project
to serve it using `NGINX <https://www.nginx.com>`_, basically you'll
need two *CNAME* in your domain, one called ``u.example.com`` to access
the API and upload files, and other called ``i.example.com`` to serve
the uploaded files as a static files server, of course you can change
them to the ones you'll like to use, also you can use
``certbot --nginx`` to support HTTPS (you should use it).
