#!/usr/bin/env bash

echo "=============== RUNNING FIXES FOR isort ==============="
python3 -m isort -y -rc src/

echo "=============== RUNNING FIXES FOR black ==============="
python3 -m black src/
