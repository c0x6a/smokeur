"""
Tests for Smokeur
"""

from fastapi.testclient import TestClient

from src.main import smokeur_api

client = TestClient(smokeur_api)

SMOKEUR_API_TOKEN = "abc-123"


def test_home():
    response = client.get("/")

    assert response.status_code == 200
    assert response.json() == {"hello": "world"}


def test_upload(tmp_path):
    header = {"X-Token": SMOKEUR_API_TOKEN}
    folder = tmp_path / "smokeur"
    folder.mkdir()
    tmp_file = folder / "smokeur_temp.txt"
    tmp_file.write_text("test content smokeur text")

    url_statics_server_test = "https://i.smokeur.app"

    with open(str(tmp_file), "rb") as file:
        files = {"file": file}
        response = client.post("/", files=files, headers=header)

    assert response.status_code == 200
    assert "result" in response.json()
    assert response.json()["result"].startswith(url_statics_server_test)


def test_upload_no_headers():
    files = {"file": b"hello"}
    response = client.post("/", files=files)

    assert response.status_code == 403
    assert response.json()["detail"] == "The access token is missing."


def test_upload_headers_no_token():
    files = {"file": b"hello"}
    headers = {"x-token": None}
    response = client.post("/", files=files, headers=headers)

    assert response.status_code == 403
    assert response.json()["detail"] == "The access token is missing."


def test_upload_headers_invalid_token():
    files = {"file": b"hello"}
    headers = {"x-token": "random-1234"}
    response = client.post("/", files=files, headers=headers)

    assert response.status_code == 403
    assert response.json()["detail"] == "The access token is invalid."


def test_upload_no_file():
    files = {"file": None}
    response = client.post("/", files=files)

    assert response.status_code == 400


def test_random_url_404():
    response = client.get("/random_url")

    assert response.status_code == 404
