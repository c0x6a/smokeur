"""
Tool to upload files

(c) 2019 - CJ
"""
import os
import uuid

from fastapi import FastAPI, File, Header, UploadFile

import ujson as json
from environs import Env
from starlette.responses import Response

env = Env()  # pylint: disable=invalid-name

env.read_env()

SMOKEUR_STATICS_SERVER = env.str("SMOKEUR_STATICS_SERVER")

SMOKEUR_MEDIA_FOLDER = env.str("SMOKEUR_MEDIA_FOLDER")

SMOKEUR_API_TOKEN = env.str("SMOKEUR_API_TOKEN")

smokeur_api = FastAPI()  # pylint: disable=invalid-name


@smokeur_api.get("/")
async def hello():
    """Just a nice hello world"""
    return {"hello": "world"}


@smokeur_api.post("/")
async def upload_file(file: UploadFile = File(...), x_token: str = Header(None)):
    """Upload a file"""
    if x_token is None:
        content = {"detail": "The access token is missing."}
        content_json = json.dumps(content)
        return Response(content_json, status_code=403)
    if x_token != SMOKEUR_API_TOKEN:
        content = {"detail": "The access token is invalid."}
        content_json = json.dumps(content)
        return Response(content_json, status_code=403)

    new_id = uuid.uuid4()
    _, file_extension = os.path.splitext(file.filename)
    file_content = await file.read()
    destination_file = f"{SMOKEUR_MEDIA_FOLDER}/{new_id}{file_extension}"
    file_handler = open(destination_file, "wb")
    file_handler.write(file_content)
    file_handler.close()
    return {"result": f"{SMOKEUR_STATICS_SERVER}/{new_id}{file_extension}"}


if __name__ == "__main__":
    # pylint: disable=pointless-string-statement,import-error
    """Use for debugging purpose only, to run as a service use
    ``uvicorn`` standalone package"""
    import uvicorn

    uvicorn.run(smokeur_api, host="127.0.0.1", port=8011)
