#!/usr/bin/env bash

echo "=============== RUNNING isort ==============="
python3 -m isort -c -rc src/

echo "=============== RUNNING black ==============="
python3 -m black --check src/

echo "=============== RUNNING pylint ==============="
python3 -m pylint src/

echo "=============== RUNNING pytest ==============="
SMOKEUR_STATICS_SERVER=https://i.smokeur.app \
SMOKEUR_MEDIA_FOLDER=/tmp \
SMOKEUR_API_TOKEN=abc-123 \
python3 -m pytest -v
