#!/usr/bin/env bash
set -xe

rsync -avzz --exclude=".*" . $SERVER_USER@$SERVER_ADDRESS:~/$PROJECT_FOLDER
ssh -p22 $SERVER_USER@$SERVER_ADDRESS "cd ~/$PROJECT_FOLDER; sh deploy/run.sh"
