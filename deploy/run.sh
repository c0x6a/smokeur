#!/usr/bin/env bash
set -xe

mv env-vars .env

if ! test -f "/home/c0x6a/nginx/smokeur.nginx"; then
    # This assumes nginx.conf includes sites enabled from your
    # user home ~/nginx/; change to the real path of your nginx
    # sites enabled.
    cp ./deploy/smokeur.nginx ~/nginx/
fi
if ! test -f "/etc/systemd/system/smokeur.socket"; then
    sudo cp ./deploy/smokeur.socket /etc/systemd/system/
fi
if ! test -f "/etc/systemd/system/smokeur.service"; then
    sudo cp ./deploy/smokeur.service /etc/systemd/system/
fi

# This script assumes you have a virtual environment in the following path,
# if not, then create one or change the path to the correct one.
~/.pyenv/versions/smokeur/bin/python3 -m pip install -U -r requirements.txt

sudo systemctl daemon-reload
sudo systemctl restart smokeur
sudo systemctl restart nginx
